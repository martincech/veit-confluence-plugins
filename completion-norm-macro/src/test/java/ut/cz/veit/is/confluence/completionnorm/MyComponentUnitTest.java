package ut.cz.veit.is.confluence.completionnorm;

import org.junit.Test;
import cz.veit.is.confluence.completionnorm.api.MyPluginComponent;
import cz.veit.is.confluence.completionnorm.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}