package cz.veit.is.confluence.completionnorm;

import static cz.veit.is.confluence.completionnorm.CompletionNormMacro.COMPLETION_NORM_CODE_PARAM;
import static cz.veit.is.confluence.completionnorm.CompletionNormMacro.COMPLETION_NORM_WEB_URL_VARIABLE;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import com.atlassian.confluence.macro.MacroExecutionException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class CompletionNormMacroTest {

    private static final String NORM_CODE = "XY1001";

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();


    private CompletionNormMacro macro = new CompletionNormMacro();

    @Test
    public void defaultWebUrlUsedIfNotSetExplicitly() throws MacroExecutionException {
        final String iframeHtml = macro.execute(singletonMap(COMPLETION_NORM_CODE_PARAM, NORM_CODE), "", null);
        assertThat(iframeHtml, containsString("http://localhost:3001/completionNorms/" + NORM_CODE));
    }

    @Test
    public void explicitWebUrlUsed() throws MacroExecutionException {
        final String customBaseUrl = "http://is.veit.cz:3001/completionNorms123";
        environmentVariables.set(COMPLETION_NORM_WEB_URL_VARIABLE, customBaseUrl);
        
        final String iframeHtml = macro.execute(singletonMap(COMPLETION_NORM_CODE_PARAM, NORM_CODE), "", null);
        assertThat(iframeHtml, containsString(customBaseUrl + "/" + NORM_CODE));
    }

}