package cz.veit.is.confluence.completionnorm;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import java.util.Map;

/**
 * Macro showing the completion norm items tree via iframe.
 * It just calls the remote web application which returns the complete html and displays that html in iframe.
 *
 * In order to work, system environment variable "CONFLUENCE_COMPLETION_NORM_BASE_URL" must be defined.
 * It can be sth like this: "http://testis.veit.cz/completionnorms"
 */
public class CompletionNormMacro implements Macro {

    /** Name of the param used in confluence macro for passing code of completion norm that should be displayed */
    public static final String COMPLETION_NORM_CODE_PARAM = "completion-norm-code";

    /** Name of envrionment variable used for getting base url where the web application which actually displays
     * data about completion norm lives. */
    public static final String COMPLETION_NORM_WEB_URL_VARIABLE = "CONFLUENCE_COMPLETION_NORM_BASE_URL";

    private static final String DEFAULT_COMPLETION_NORM_BASE_URL = "http://localhost:3001/completionNorms";

    public String execute(Map<String, String> parameters, String s, ConversionContext conversionContext) throws MacroExecutionException {
        return String.format(
                "<iframe src=\"%s/%s\" frameborder=\"0\" onload=\"this.height=screen.height;\" width=\"100%%\" scrolling=\"yes\"></iframe>",
                getCompletionNormsBaseUrl(),
                parameters.get(COMPLETION_NORM_CODE_PARAM));
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }


    private static String getCompletionNormsBaseUrl() {
        final String urlFromEnv = System.getenv(COMPLETION_NORM_WEB_URL_VARIABLE);
        if (urlFromEnv == null || urlFromEnv.length() == 0) {
            return DEFAULT_COMPLETION_NORM_BASE_URL;
        }
        return urlFromEnv;
    }
}
