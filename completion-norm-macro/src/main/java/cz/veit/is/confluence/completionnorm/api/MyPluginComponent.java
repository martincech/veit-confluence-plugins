package cz.veit.is.confluence.completionnorm.api;

public interface MyPluginComponent
{
    String getName();
}