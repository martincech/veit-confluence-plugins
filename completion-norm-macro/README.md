# VEIT Completion Norm macro for Confluence

## Overview

Completion Norm Macro can be used to embed Completion Norm items tree into the Confluence page.

Macro inserts HTML produced by separate web application running on the server.
This web application is deployed here: http://testis.veit.cz/completionnorms/<completion-norm-code>

The single required parameter for the macro is "completion-norm-code".

## Deployment

Prerequisites:
* [Set up the Atlassian Plugin SDK and Build a Project](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project)

The standard procedure as for any other confluence macro applies:

1. Build macro: `atlas-mvn clean package`
2. Set environment variable CONFLUENCE_COMPLETION_NORM_BASE_URL in /etc/systemd/system/confluence.service
```
Environment="CONFLUENCE_COMPLETION_NORM_BASE_URL=http://testis.veit.cz/completionNorms"
```
3. Upload to confluence (Settings -> Manage add-ons -> Upload add-on -> From my computer -> Select target/*.obr file)

## Internationalization

Properties files for English and Czech are stored in src/main/resources directory.